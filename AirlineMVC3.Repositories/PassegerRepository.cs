﻿using AirlineMVC3.DAL;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace AirlineMVC3.Repositories
{
    public class PassegerRepository : IRepository<Passenger>
    {
        private readonly AirlineDBContext context;
        private readonly DbSet<Passenger> dbSet;

        public PassegerRepository(AirlineDBContext cntx = null)
        {
            if(cntx == null)
            {
                this.context = new AirlineDBContext();
                this.dbSet = context.Set<Passenger>();
            }
        }

        public void Create(Passenger entity)
        {
            this.dbSet.Add(entity);
        }

        public void Delete(int id)
        {
            var innerEnt = this.Get(id);
            this.dbSet.Remove(innerEnt);
        }

        public Passenger Get(int id)
        {
            return this.dbSet.First(pass => pass.PassengerId == id);
        }

        public IEnumerable<Passenger> GetAll()
        {
            return this.dbSet.ToArray();
        }

        public DbSet<Passenger> GetSet()
        {
            return this.dbSet;
        }

        public void SaveChanges()
        {
            context.SaveChanges();
        }

        public void Update(Passenger entity)
        {
            var innerEnt = this.Get(entity.PassengerId);

            innerEnt.FirstName = entity.FirstName;
            innerEnt.LastName = entity.LastName;
            innerEnt.Nationality = entity.Nationality;
            innerEnt.FlightNumber = entity.FlightNumber;
            innerEnt.BirthDate = entity.BirthDate;
            innerEnt.GenderId = entity.GenderId;
            innerEnt.ClassId = entity.ClassId;
            innerEnt.CurrFlight = entity.CurrFlight;

            this.context.Entry<Passenger>(innerEnt).State = EntityState.Modified;
        }
    }
}
