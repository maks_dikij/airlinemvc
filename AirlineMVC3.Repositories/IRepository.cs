﻿using System.Collections.Generic;
using System.Data.Entity;

namespace AirlineMVC3.Repositories
{
    public interface IRepository<TData>
        where TData : class
    {
        DbSet<TData> GetSet();
        TData Get(int id);
        void Create(TData entity);
        IEnumerable<TData> GetAll();
        void Update(TData entity);
        void Delete(int id);
        void SaveChanges();
    }
}
