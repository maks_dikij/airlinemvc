﻿using AirlineMVC3.DAL;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace AirlineMVC3.Repositories
{
    public class FlightRepository : IRepository<Flight>
    {
        private readonly AirlineDBContext context;
        public readonly DbSet<Flight> dbSet;

        public FlightRepository() : this(null)
        {

        }

        public FlightRepository(AirlineDBContext cntxt = null)
        {
            if(cntxt == null)
            {
                this.context = new AirlineDBContext();
                this.dbSet = context.Set<Flight>();
            }
        }

        public void Create(Flight entity)
        {
            this.dbSet.Add(entity);
        }

        public void Delete(int id)
        {
            var innerEnt = this.Get(id);
            this.dbSet.Remove(innerEnt);
        }

        public Flight Get(int id)
        {
            return this.dbSet.First(flt => flt.FlightNumber == id);
        }

        public IEnumerable<Flight> GetAll()
            => this.dbSet.ToArray();

        public void SaveChanges()
        {
            context.SaveChanges();
        }

        public void Update(Flight entity)
        {
            var innerEnt = this.Get(entity.FlightNumber);

            innerEnt.FlightType = entity.FlightType;
            innerEnt.Date = entity.Date;
            innerEnt.Port = entity.Port;
            innerEnt.Terminal = entity.Terminal;
            innerEnt.Company = entity.Company;
            innerEnt.BusinessPrice = entity.BusinessPrice;
            innerEnt.EconomPrice = entity.EconomPrice;
            innerEnt.FlightStatus = entity.FlightStatus;

            this.context.Entry(innerEnt).State = EntityState.Modified;

        }

        DbSet<Flight> IRepository<Flight>.GetSet()
        {
            return this.dbSet;
        }
    }
}
