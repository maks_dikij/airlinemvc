﻿CREATE DATABASE AirlineTestDB
GO

USE AirlineTestDB
GO

CREATE TABLE dbo.Flights
(
flightnumber INT NOT NULL IDENTITY(1,1),
flighttype INT NOT NULL,
fltdate DATETIME NOT NULL,
fltport NVARCHAR(255) NOT NULL,
terminal INT NOT NULL,
company NVARCHAR(255),
businessprice MONEY NOT NULL,
economprice MONEY NOT NULL,
flightstatus INT NOT NULL,

CONSTRAINT PK_Flights PRIMARY KEY (flightnumber),
CONSTRAINT CHK_Status CHECK (flightstatus BETWEEN 0 AND 8),
);

CREATE TABLE dbo.Passengers
(
id INT	NOT NULL IDENTITY(1,1),
passport NVARCHAR(255) NOT NULL UNIQUE,
firstname NVARCHAR(255) NOT NULL,
lastname NVARCHAR(255) NOT NULL,
nationality NVARCHAR(255) NOT NULL,
flightnumber INT,
birthdate DATETIME NOT NULL,
genderid INT NOT NULL,
classid INT

CONSTRAINT PK_Passengers PRIMARY KEY (id),
CONSTRAINT FK_Flight FOREIGN KEY (flightnumber) REFERENCES dbo.Flights(flightnumber),
CONSTRAINT CHK_Gender CHECK(genderid IN (1, 2, 3)),
CONSTRAINT CHK_ClassId CHECK(classid IN (NULL, 1, 2))
);