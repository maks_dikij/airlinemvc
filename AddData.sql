﻿USE AirlineTestDB
GO

INSERT INTO dbo.Flights(flighttype, fltdate, fltport, terminal, company, businessprice, economprice, flightstatus) 
VALUES (0, '20211021 21:00', N'Kyiv', 3, N'Mau', 200, 500, 8),
	(1, '20211022 22:00', N'Kyiv', 2, N'Mau', 300, 600, 6),
	(1, '20211023 23:00', N'Kyiv', 2, N'Mau', 400, 700, 6),
	(0, '20211024', N'Odesa', 1, N'FooAir', 150, 100, 2);
GO

INSERT INTO dbo.Passengers(passport, firstName, lastname, nationality, birthDate, genderId, classid, flightnumber)
VALUES ('Passcode01', N'Dummy', N'Test', N'Ukrainian', '19910101', 1 ,2, 1),
	('Passcode02', N'Foo', N'Test', N'Ukrainian', '19920202', 1, 2, 1),
	('Passcode03', N'Third', N'Test2 ', N'Ukrainian', '19930303', 2, 1, 2),
	('Passcode04', N'Oleg', N'NoData', N'Ukrainian', '19940404', 2, NULL, NULL),
	('Passcode05', N'Igor', N'TestDate', N'Ukrainian', '19950505', 3, 1, 3),
	('Passcode06', N'Max', N'Min', N'Ukrainian', '19960121', 1, 2, 3)
GO

