using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AirlineMVC3.DAL
{
    [Table("Flights")]
    public partial class Flight
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Flight()
        {
            Passengers = new HashSet<Passenger>();
        }

        [Key]
        [Column("flightnumber")]
        public int FlightNumber { get; set; }

        [Column("flighttype")]
        public int FlightType { get; set; }
        [Column("fltdate")]
        public DateTime Date { get; set; }

        [Required]
        [StringLength(255)]
        [Column("fltport")]
        public string Port { get; set; }
        [Column("terminal")]
        public int Terminal { get; set; }

        [StringLength(255)]
        [Column("company")]
        public string Company { get; set; }

        [Column("businessprice",TypeName = "money")]
        public decimal BusinessPrice { get; set; }

        [Column("economprice", TypeName = "money")]
        public decimal EconomPrice { get; set; }

        [Column("flightstatus")]
        public int FlightStatus { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Passenger> Passengers { get; set; }
    }
}
