using System.Data.Entity;

namespace AirlineMVC3.DAL
{

    public partial class AirlineDBContext : DbContext
    {
        public AirlineDBContext()
            : base("data source=DESKTOP-B73LU0H\\SQLEXPRESS;initial catalog=AirlineTestDB;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework")
        {
        }

        public virtual DbSet<Flight> Flights { get; set; }
        public virtual DbSet<Passenger> Passengers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Flight>()
                .Property(e => e.BusinessPrice)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Flight>()
                .Property(e => e.BusinessPrice)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Passenger>()
                .HasOptional<Flight>(pass => pass.CurrFlight)
                .WithMany(flt => flt.Passengers)
                .HasForeignKey(pass => pass.FlightNumber)
                .WillCascadeOnDelete(false);

        }
    }
}
