using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AirlineMVC3.DAL
{

    [Table("Passengers")]
    public partial class Passenger
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("id", Order = 1)]
        public int PassengerId { get; set; }

        [Required]
        [StringLength(255)]
        [Column("passport")]
        public string Passport { get; set; }

        [Required]
        [StringLength(255)]
        [Column("firstname")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(255)]
        [Column("lastname")]
        public string LastName { get; set; }

        [Required]
        [StringLength(255)]
        [Column("nationality")]
        public string Nationality { get; set; }

        [Column("flightnumber")]
        public int? FlightNumber { get; set; }

        [Column("birthdate")]
        public DateTime BirthDate { get; set; }

        [Column("genderid")]
        public int GenderId { get; set; }

        [Column("classid")]
        public int? ClassId { get; set; }

        [Column("currflight")]
        public virtual Flight CurrFlight { get; set; }
    }
}
