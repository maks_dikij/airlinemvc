﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AirlineMVC3.BL;
using AirlineMVC3.Models;

namespace AirlineMVC3.Controllers
{
    public class FlightsController : Controller
    {
        private IFlightService flightService;
        private IPassengerService passengerService;

        public FlightsController()
        {
            this.flightService = new FlightService();
            this.passengerService = new PassengerService();
        }
        // GET: Flights
        public ActionResult Index(string searchBy, string search, string searchOutputType)
        {
            IServiceResult<IEnumerable<FlightBLModel>> serviceResult;

            Func<FlightBLModel, bool> searchOutputPredicate = flight
                => true;

            if (search == string.Empty) searchBy = string.Empty;

            switch (searchBy)
            {
                case "FlightNumber":
                    serviceResult = this.flightService.GetFlightByNumber(search);
                    break;
                case "Port":
                    serviceResult = this.flightService.GetFlightsByPort(search);
                    break;
                default:
                    serviceResult = this.flightService.GetAll();
                    break;
            }

            switch (searchOutputType)
            {
                case "Departure":
                    searchOutputPredicate = flight
                        => flight.FlightType == FlightType.Departure;
                    break;
                case "Arrival":
                    searchOutputPredicate = flight
                        => flight.FlightType == FlightType.Arrival;
                    break;
                default:
                    break;
            }

            if (!serviceResult.IsSuccess)
            {
                return Redirect("Errors/Error");
            }

            var flightViewList = serviceResult.Data
                .Where(searchOutputPredicate)
                .Select(flt => FlightsController.convertToVM(flt))
                .ToList();

            return View(flightViewList);
        }

        public ActionResult Edit(int? id)
        {

            if (!id.HasValue)
            {
                return Redirect("Errors/PageDoesNotExist");
            }

            
            var flightServiceResp = this.flightService.Get(id.Value);

            if (!flightServiceResp.IsSuccess)
            {
                return Redirect("Errors/Error");
            }

            var fltView = FlightsController.convertToVM(flightServiceResp.Data);

            return View(fltView);           
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FlightViewModel fltView)
        {
            if (!ModelState.IsValid)
            {
                return View(fltView);
            }

            var serviceResp = this.flightService.Update(FlightsController.convertToBL(fltView));

            if (!serviceResp.IsSuccess)
            {
                return Redirect("Errors/Error");
            }

            return RedirectToAction("Index");
        }

        public ActionResult Details(int? id)
        {
            if (!id.HasValue)
            {
                return Redirect("Errors/Error");
            }

            var serviceResponce = this.flightService.Get(id.Value);

            if (!serviceResponce.IsSuccess)
            {
                return Redirect("Errors/Error"); 
            }

            var passServiceResponce = this.passengerService.GetAll();

            if (!passServiceResponce.IsSuccess)
            {
                ViewData["passengers"] = null;
            } else
            {
                ViewData["passengers"] = passServiceResponce.Data
                    .Where(pass => pass.FlightNumber == id)
                    .Select(pass => PassengersController.convertToVM(pass))
                    .ToList();
            }

            return View(FlightsController.convertToVM(serviceResponce.Data));
        }

        public ActionResult Create()
        {
            return View(new FlightViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Exclude = "FlightNumber")] FlightViewModel flt)
        {
            if (!ModelState.IsValid)
            {
                return Redirect("../Errors/Error");
            }

            var serviceResponce = flightService.Create(convertToBL(flt));

            if (!serviceResponce.IsSuccess)
            {
                return View();
            }

            return RedirectToAction("Index");
        }

        public ActionResult Delete(int? id)
        {
            if (!id.HasValue)
            {
                return Redirect("../Errors/Error");
            }

            var serviceResponce = this.flightService.Get(id.Value);

            if (!serviceResponce.IsSuccess)
            {
                return Redirect("../Errors/PageDoesNotExist");
            }

            ViewBag.ErrorMessage = null;

            var passServiceResponce = this.passengerService.GetAll();

            if (!passServiceResponce.IsSuccess)
            {
                ViewBag.PassengerList = null;
            }
            else
            {
                ViewBag.PassengerList = passServiceResponce.Data
                    .Where(pass => pass.FlightNumber == id)
                    .Select(pass => PassengersController.convertToVM(pass))
                    .ToList();
            }

            var flight = FlightsController.convertToVM(serviceResponce.Data);

            return View(flight);


        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(FlightViewModel flight)
        {
            var serviceResponce = this.flightService.Delete(flight.FlightNumber);

            if (!serviceResponce.IsSuccess)
            {
                var passServiceResponce = this.passengerService.GetAll();

                if (!passServiceResponce.IsSuccess)
                {
                    ViewBag.PassengerList = null;
                }
                else
                {
                    ViewBag.PassengerList = passServiceResponce.Data
                        .Where(pass => pass.FlightNumber == flight.FlightNumber)
                        .Select(pass => PassengersController.convertToVM(pass))
                        .ToList();
                }

                ViewBag.ErrorMessage = "Error: Can't delete flight with passengers. Delete passengers from the flight first";

                return View(flight);
            }

            return RedirectToAction("Index");
        }


        public static FlightViewModel convertToVM(FlightBLModel flt)
        {
            return new FlightViewModel(
                flt.FlightNumber,
                flt.FlightType,
                flt.FLDate,
                flt.Port,
                flt.Terminal,
                flt.Company,
                flt.BusinessPrice,
                flt.EconomPrice,
                flt.FlightStatus);
        }

        public static FlightBLModel convertToBL(FlightViewModel flt)
        {
            return new FlightBLModel(
                flt.FlightNumber,
                flt.FlightType,
                flt.FLDate,
                flt.Port,
                flt.Terminal,
                flt.Company,
                flt.BusinessPrice,
                flt.EconomPrice,
                flt.FlightStatus);
        }
    }
}