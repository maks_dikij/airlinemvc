﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AirlineMVC3.BL;
using AirlineMVC3.Models;

namespace AirlineMVC3.Controllers
{
    public class PassengersController : Controller
    {
        IFlightService flightService;
        IPassengerService passengerService;
        SelectList priceClassOptions;

        public PassengersController()
        {
            this.flightService = new FlightService();
            this.passengerService = new PassengerService();
            this.priceClassOptions = new SelectList(new List<PriceClass?> { null, PriceClass.Business, PriceClass.Econom });
        }

        // GET: Passengers
        [ActionName("IndexInitial")]
        public ActionResult Index()
        {
            var passServiceResponce = this.passengerService.GetAll();

            if (!passServiceResponce.IsSuccess)
            {
                return Redirect("../Errors/Error");
            }

            var passengers = passServiceResponce.Data
                .Select(pass => PassengersController.convertToVM(pass))
                .ToList();


            return View(passengers);
        }

        public ActionResult Index(string search, string searchBy)
        {
            IServiceResult<IEnumerable<PassengerBLModel>> passServiceResponce;

            if (search == string.Empty) searchBy = string.Empty;

            switch (searchBy)
            {
                case "FirstName":
                    passServiceResponce = passengerService.GetPassengerByFirstName(search);
                    break;
                case "LastName":
                    passServiceResponce = passengerService.GetPassengerByLastName(search);
                    break;
                case "Passport":
                    passServiceResponce = passengerService.GetPassengerByPassport(search);
                    break;
                case "FlightNumber":
                    passServiceResponce = passengerService.GetPassengersByFlightNumber(search);
                    break;
                default:
                    passServiceResponce = this.passengerService.GetAll();
                    break;
            }

            if (!passServiceResponce.IsSuccess)
            {
                return Redirect("../Errors/Errors");
            }

            var pasengers = passServiceResponce.Data
                .Select(pass => PassengersController.convertToVM(pass))
                .ToList();

            return View(pasengers);
        }

        public ActionResult Details(int? id)
        {
            if (!id.HasValue)
            {
                return Redirect("../Errors/Error");
            }

            var passServiceResponce = this.passengerService.Get(id.Value);

            if (!passServiceResponce.IsSuccess)
            {
                return Redirect("../Errors/PageDoesNotExist");
            }

            var passenger = PassengersController.convertToVM(passServiceResponce.Data);

            ViewBag.CurrentFlight = new List<FlightViewModel>();

            if (passenger.FlightNumber.HasValue)
            {
                var flightServiceResponce = this.flightService.Get(passServiceResponce.Data.FlightNumber.Value);

                if (flightServiceResponce.IsSuccess)
                {
                    ViewBag.CurrentFlight.Add(FlightsController.convertToVM(flightServiceResponce.Data));
                }

            }

            return View(passenger);
        }

        public ActionResult Edit(int? id)
        {
            if (!id.HasValue || id == null)
            {
                return Redirect("../Errors/PageDoesNotExists");
            }

            var passServiceResponce = this.passengerService.Get(id.Value);
          
            var passenger = PassengersController.convertToVM(passServiceResponce.Data);

            ViewBag.FlightNums = this.getFlightNums();
            ViewBag.ClassList = priceClassOptions;

            return View(passenger);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PassengerViewModel pass)
        {
            if((pass.FlightNumber == null && pass.Class != null))
            {
                ModelState.AddModelError("Class", "Class shoul be null if flight number is null");
            }

            if((pass.FlightNumber != null && pass.Class == null))
            {
                ModelState.AddModelError("Class", "Class shouldn't be null if flight number in not null");
            }

            if (!ModelState.IsValid)
            {
                ViewBag.FlightNums = this.getFlightNums();
                ViewBag.ClassList = this.priceClassOptions;
                return View(pass);
            }

            var serviceResponce = this.passengerService.Update(PassengersController.convertToBL(pass));

            if (!serviceResponce.IsSuccess)
            {
                return Redirect("../Errors/Error");
            }

            return RedirectToAction("Index");
        }

        public ActionResult Create()
        {
            ViewBag.FlightNums = this.getFlightNums();
            ViewBag.ClassList = this.priceClassOptions;

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Exclude = "PassengerId")] PassengerViewModel pass)
        {

            if (!ModelState.IsValid)
            {
                ViewBag.FlightNums = this.getFlightNums();
                ViewBag.ClassList = this.priceClassOptions;
                return View(pass);
            }

            var passServiceResponce = this.passengerService.Create(PassengersController.convertToBL(pass));

            if (!passServiceResponce.IsSuccess)
            {
                return Redirect("../Errors/Error");
            }

            return RedirectToAction("Index");
        }
      
        public ActionResult Delete(int? id)
        {
            if (!id.HasValue)
            {
                return Redirect("../Errors/PageDoesNotExists");
            }

            var serviceResponce = this.passengerService.Get(id.Value);

            if (!serviceResponce.IsSuccess)
            {
                return Redirect("../Errors/Error");
            }

            var passenger = PassengersController.convertToVM(serviceResponce.Data);

            return View(passenger);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(PassengerViewModel pass)
        {
            this.passengerService.Delete(pass.PassengerId);

            return RedirectToAction("Index");
        }

        public static PassengerViewModel convertToVM(PassengerBLModel pass)
        {
            return new PassengerViewModel(pass.PassengerId,
                pass.Passport,
                pass.FirstName,
                pass.LastName,
                pass.Nationality,
                pass.FlightNumber,
                pass.BirthDate,
                pass.Gender,
                pass.Class);
        }

        public static PassengerBLModel convertToBL(PassengerViewModel pass)
        {
            return new PassengerBLModel(pass.PassengerId,
                pass.Passport,
                pass.FirstName,
                pass.LastName,
                pass.Nationality,
                pass.FlightNumber,
                pass.BirthDate,
                pass.Gender,
                pass.Class);
        }

        private SelectList getFlightNums()
        {
            var fltServiceResponce = this.flightService.GetAll();

            if (!fltServiceResponce.IsSuccess)
            {
                return new SelectList(new int?[] { });
            }
            else
            {
                return new SelectList(fltServiceResponce.Data
                    .Select(flt => (int?)flt.FlightNumber)
                    .ToArray()
                    .Prepend(null));
            }
        }
    }
}