﻿using System;
using System.ComponentModel.DataAnnotations;
using AirlineMVC3.BL;

namespace AirlineMVC3.Models
{
    public class PassengerViewModel
    {
        [Display(Name="Passenger Id")]
        public int PassengerId { get; set; }

        [Display(Name="Passport Code")]
        public string Passport { get; set; }

        [Display(Name="First Name")]
        public string FirstName { get; set; }

        [Display(Name="Last Name")]
        public string LastName { get; set; }

        [Display(Name="Nationality")]
        public string Nationality { get; set; }

        [Display(Name="Flight Number")]
        public int? FlightNumber { get; set; }

        [Display(Name="Birth Date")]
        public DateTime BirthDate { get; set; }

        [Display(Name="Gender")]
        public Gender Gender { get; set; }

        [Display(Name ="Price Class")]
        public PriceClass? Class { get; set; }

        public PassengerViewModel()
        {

        }

        public PassengerViewModel(int passId, string passport, string firstName, string lastName, string nationality, int? flightNumber, DateTime birtDate, Gender gender, PriceClass? prClass)
        {
            this.PassengerId = passId;
            this.Passport = passport;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Nationality = nationality;
            this.FlightNumber = flightNumber;
            this.BirthDate = birtDate;
            this.Gender = gender;
            this.Class = prClass;
        }

    }
}