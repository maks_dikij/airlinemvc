﻿using System;
using AirlineMVC3.BL;

namespace AirlineMVC3.Models
{
    public class PassangerViewModel
    {
        public string Passport { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Nationality { get; set; }

        public int? FlightNumber { get; set; }

        public DateTime BirthDate { get; set; }

        public Gender Gender { get; set; }

        public PriceClass Class { get; set; }

        public PassangerViewModel(string passport, string firstName, string nationality, int? flightNumber, DateTime birtDate, Gender gender, PriceClass prClass)
        {
            this.Passport = passport;
            this.FirstName = firstName;
            this.Nationality = nationality;
            this.BirthDate = birtDate;
            this.Gender = gender;
            this.Class = prClass;
        }

    }
}