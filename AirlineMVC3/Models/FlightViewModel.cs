﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AirlineMVC3.BL;

namespace AirlineMVC3.Models
{
    public class FlightViewModel
    {
        [Display(Name ="Flight Number")]
        public int FlightNumber { get; set; }

        [Display(Name="Flight Type")]
        public FlightType FlightType { get; set; }

        [Display(Name ="Date Time")]
        public DateTime FLDate { get; set; }

        [Display(Name ="Port")]
        public string Port { get; set; }

        [Display(Name ="Terminal")]
        public int Terminal { get; set; }

        [Display(Name ="Company")]
        public string Company { get; set; }

        [Display(Name="Business Price")]
        public decimal BusinessPrice { get; set; }

        [Display(Name="Econom Price")]
        public decimal EconomPrice { get; set; }

        [Display(Name="Flight Status")]
        public FlightStatus FlightStatus { get; set; }

        public FlightViewModel()
        {

        }

        public FlightViewModel(int fltNum, FlightType fltType, DateTime date, string port, int terminal, string company, decimal bisPrice, decimal ecPrice, FlightStatus status)
        {
            this.FlightNumber = fltNum;
            this.FlightType = fltType;
            this.FLDate = date;
            this.Terminal = terminal;
            this.Port = port;
            this.Company = company;
            this.BusinessPrice = bisPrice;
            this.EconomPrice = ecPrice;
            this.FlightStatus = status;
        }
    }
}