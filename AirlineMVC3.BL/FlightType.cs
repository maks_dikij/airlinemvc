﻿namespace AirlineMVC3.BL
{
    public enum FlightType
    {
        Arrival,
        Departure
    }
}
