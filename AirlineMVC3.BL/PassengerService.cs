﻿using AirlineMVC3.DAL;
using AirlineMVC3.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AirlineMVC3.BL
{
    public class PassengerService : IPassengerService
    {
        private readonly IRepository<Passenger> repository;

        public PassengerService(IRepository<Passenger> repos = null)
        {
            this.repository = repos == null ? new PassegerRepository() : repos;
        }

        public IServiceResult<PassengerBLModel> Create(PassengerBLModel entity)
        {
            try
            {
                this.repository.Create(this.convertToPassenger(entity));
                this.repository.SaveChanges();
                return new ServiceResult<PassengerBLModel>(true, entity);
            }
            catch(Exception ex)
            {
                return new ServiceResult<PassengerBLModel>(false, entity, ex);
            }
        }

        public IServiceResult<PassengerBLModel> Delete(int id)
        {
            try
            {
                this.repository.Delete(id);
                this.repository.SaveChanges();
                return new ServiceResult<PassengerBLModel>(true, null);
            }
            catch(Exception ex)
            {
                return new ServiceResult<PassengerBLModel>(false, null, ex);
            }
        }

        public IServiceResult<PassengerBLModel> Get(int id)
        {
            try
            {
                var pass = this.repository.Get(id);
                var model = this.convertToBLModel(pass);
                return new ServiceResult<PassengerBLModel>(true, model);
            }
            catch(Exception ex)
            {
                return new ServiceResult<PassengerBLModel>(false, null, ex);
            }
            
        }

        public IServiceResult<IEnumerable<PassengerBLModel>> GetAll()
        {
            try
            {
                var passengers = this.repository.GetAll()
                 .Select(pass => this.convertToBLModel(pass));

                return new ServiceResult<IEnumerable<PassengerBLModel>>(true, passengers);
            }
            catch(Exception ex)
            {
                return new ServiceResult<IEnumerable<PassengerBLModel>>(false, null, ex);
            }
               
        }

        public IServiceResult<PassengerBLModel> Update(PassengerBLModel entity)
        {
            try
            {
                this.repository.Update(this.convertToPassenger(entity));
                this.repository.SaveChanges();
                return new ServiceResult<PassengerBLModel>(true, entity);
            }
            catch(Exception ex)
            {
                return new ServiceResult<PassengerBLModel>(false, entity, ex);
            }
        }

        public IServiceResult<IEnumerable<PassengerBLModel>> GetPassengersByFlightNumber(string fltNum)
        {
            try
            {
                var passengers = this.repository.GetSet()
                    .Where(pass => pass.FlightNumber.ToString().Contains(fltNum.ToString()))
                    .ToList()
                    .Select(pass => this.convertToBLModel(pass))
                    .ToList();

                return ServiceResult<IEnumerable<PassengerBLModel>>.Success(passengers);
            }
            catch(Exception ex)
            {
                return ServiceResult<IEnumerable<PassengerBLModel>>.Failure(ex);
            }
        }

        public IServiceResult<IEnumerable<PassengerBLModel>> GetPassengerByPassport(string passport)
        {
            try
            {
                var passengers = this.repository.GetSet()
                    .Where(pass => pass.Passport.ToLower().Contains(passport.ToLower()))
                    .ToList()
                    .Select(pass => this.convertToBLModel(pass))
                    .ToList();

                return ServiceResult<IEnumerable<PassengerBLModel>>.Success(passengers);
            }
            catch(Exception ex)
            {
                return ServiceResult<IEnumerable<PassengerBLModel>>.Failure(ex);
            }
        }

        public IServiceResult<IEnumerable<PassengerBLModel>> GetPassengerByFirstName(string fName)
        {
            try
            {
                var passengers = this.repository.GetSet()
                    .Where(pass => pass.FirstName.ToLower().Contains(fName.ToLower()))
                    .ToList()
                    .Select(pass => this.convertToBLModel(pass))
                    .ToList();

                return ServiceResult<IEnumerable<PassengerBLModel>>.Success(passengers);
            }
            catch(Exception ex)
            {
                return ServiceResult<IEnumerable<PassengerBLModel>>.Failure(ex);
            }
        }


        public IServiceResult<IEnumerable<PassengerBLModel>> GetPassengerByLastName(string lName)
        {
            try
            {
                var passengers = this.repository.GetSet()
                    .Where(pass => pass.LastName.ToLower().Contains(lName.ToLower()))
                    .ToList()
                    .Select(pass => this.convertToBLModel(pass))
                    .ToList();

                return ServiceResult<IEnumerable<PassengerBLModel>>.Success(passengers);
            }
            catch (Exception ex)
            {
                return ServiceResult<IEnumerable<PassengerBLModel>>.Failure(ex);
            }
        }


        public Passenger convertToPassenger(PassengerBLModel model)
        {
            return new Passenger
            {
                PassengerId = model.PassengerId,
                Passport = model.Passport,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Nationality = model.Nationality,
                FlightNumber = model.FlightNumber,
                BirthDate = model.BirthDate,
                GenderId = (int)model.Gender,
                ClassId = (int?)model.Class,
            };
        }

        public PassengerBLModel convertToBLModel(Passenger pass)
        {
            return new PassengerBLModel
            {
                PassengerId = pass.PassengerId,
                Passport = pass.Passport,
                FirstName = pass.FirstName,
                LastName = pass.LastName,
                Nationality = pass.Nationality,
                FlightNumber = pass.FlightNumber,
                BirthDate = pass.BirthDate,
                Gender = (Gender)pass.GenderId,
                Class = (PriceClass?)pass.ClassId
            };
        }

    }
}
