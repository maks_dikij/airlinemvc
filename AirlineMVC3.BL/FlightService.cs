﻿using AirlineMVC3.DAL;
using AirlineMVC3.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AirlineMVC3.BL
{
    public class FlightService : IFlightService
    {
        private readonly IRepository<Flight> respository;

        public FlightService() : this(null)
        {

        }

        public FlightService(IRepository<Flight> repos = null)
        {
            this.respository = repos == null ? new FlightRepository() : repos;
        }

        public IServiceResult<FlightBLModel> Create(FlightBLModel entity)
        {
            

            try
            {
                var flight = this.converToFlight(entity);
                this.respository.Create(flight);
                this.respository.SaveChanges();
                return new ServiceResult<FlightBLModel>(true, entity);
            }
            catch(Exception ex)
            {
                return new ServiceResult<FlightBLModel>(false, entity, ex);
            }

            
        }

        public IServiceResult<FlightBLModel> Delete(int id)
        {
            try
            {
                this.respository.Delete(id);
                this.respository.SaveChanges();
                return new ServiceResult<FlightBLModel>(true, null);
            }
            catch(Exception ex)
            {
                return new ServiceResult<FlightBLModel>(false, null, ex);
            }
        }

        public IServiceResult<FlightBLModel> Get(int id)
        {
            try
            {
                var flight = this.respository.Get(id);
                var fltModel = new FlightBLModel
                {
                    FlightNumber = flight.FlightNumber,
                    FlightType = (FlightType)flight.FlightType,
                    FLDate = flight.Date,
                    Port = flight.Port,
                    Terminal = flight.Terminal,
                    Company = flight.Company,
                    BusinessPrice = flight.BusinessPrice,
                    EconomPrice = flight.EconomPrice,
                    FlightStatus = (FlightStatus) flight.FlightStatus
                };

                this.respository.SaveChanges();
                return ServiceResult<FlightBLModel>.Success(fltModel);
            }
            catch(Exception ex)
            {
                return ServiceResult<FlightBLModel>.Failure(ex);
            }
        }

        public IServiceResult<IEnumerable<FlightBLModel>> GetAll()
        {
            try
            {
                var flights = this.respository.GetAll()
                   .Select(flt => this.convertToBLModel(flt));

                return ServiceResult<IEnumerable<FlightBLModel>>.Success(flights);

            }
            catch(Exception ex)
            {
                return ServiceResult<IEnumerable<FlightBLModel>>.Failure(ex);
            }            
        }

        public IServiceResult<IEnumerable<FlightBLModel>> GetFlightByNumber(string id)
        {
            try
            {
                var rawFlights = this.respository.GetSet()
                    .Where(flt => flt.FlightNumber.ToString().Contains(id.ToString()))
                    .ToList();

                var flights = rawFlights
                    .Select(flt => this.convertToBLModel(flt))
                    .ToList();

                return ServiceResult<IEnumerable<FlightBLModel>>.Success(flights);
            }
            catch(Exception ex)
            {
                return ServiceResult<IEnumerable<FlightBLModel>>.Failure(ex);
            }
        }

        public IServiceResult<IEnumerable<FlightBLModel>> GetFlightByPrice(string price)
        {
            try
            {
                var rawFlights = this.respository.GetSet()
                    .Where(flt => flt.BusinessPrice.ToString().Contains(price.ToString()) || flt.EconomPrice.ToString().Contains(price.ToString()))
                    .ToList();

                var flights = rawFlights
                    .Select(flt => this.convertToBLModel(flt))
                    .ToList();

                return ServiceResult<IEnumerable<FlightBLModel>>.Success(flights);
            }
            catch (Exception ex)
            {
                return ServiceResult<IEnumerable<FlightBLModel>>.Failure(ex);
            }
        }

        public IServiceResult<IEnumerable<FlightBLModel>> GetFlightsByPort(string port)
        {
            try
            {
                var rawFlights = this.respository.GetSet()
                    .Where(flt => flt.Port.ToLower().Contains(port.ToLower()))
                    .ToList();

                var flights = rawFlights
                    .Select(flt => this.convertToBLModel(flt))
                    .ToList();

                return ServiceResult<IEnumerable<FlightBLModel>>.Success(flights);
            }
            catch(Exception ex)
            {
                return ServiceResult<IEnumerable<FlightBLModel>>.Failure(ex);
            }

        }

        public IServiceResult<FlightBLModel> Update(FlightBLModel entity)
        {
            try
            {
                var flight = this.converToFlight(entity);
                this.respository.Update(flight);
                this.respository.SaveChanges();
                return new ServiceResult<FlightBLModel>(true, entity);
            }
            catch(Exception ex)
            {
                return new ServiceResult<FlightBLModel>(false, entity, ex);
            }
        }

        private Flight converToFlight(FlightBLModel model)
        {
            return new Flight
            {
                FlightNumber = model.FlightNumber,
                FlightType = (int) model.FlightType,
                Date = model.FLDate,
                Port = model.Port,
                Terminal = model.Terminal,
                Company = model.Company,
                BusinessPrice = model.BusinessPrice,
                EconomPrice = model.EconomPrice,
                FlightStatus = (int)model.FlightStatus
            };
        }

        private FlightBLModel convertToBLModel(Flight flight)
        {
            return new FlightBLModel
            {
                FlightNumber = flight.FlightNumber,
                FlightType = (FlightType)flight.FlightType,
                FLDate = flight.Date,
                Port = flight.Port,
                Terminal = flight.Terminal,
                Company = flight.Company,
                BusinessPrice = flight.BusinessPrice,
                EconomPrice = flight.EconomPrice,
                FlightStatus = (FlightStatus)flight.FlightStatus
            };
        }
    }

}
