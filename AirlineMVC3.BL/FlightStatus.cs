﻿namespace AirlineMVC3.BL
{
    public enum FlightStatus
    {
        CheckIn,
        Gate_Closed,
        Arrived,
        Departed_At,
        Unknow,
        Cancelled,
        Expected_At,
        Delayed,
        In_Flight
    }
}
