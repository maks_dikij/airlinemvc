﻿using System;

namespace AirlineMVC3.BL
{
    public class PassengerBLModel
    {
        public PassengerBLModel()
        {

        }

        public PassengerBLModel(int passId, string passport, string firstName, string lastName, string nationality, int? fltNumber, DateTime birthDate, Gender gender, PriceClass? priceClass)
        {
            this.PassengerId = passId;
            this.Passport = passport;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Nationality = nationality;
            this.FlightNumber = fltNumber;
            this.BirthDate = birthDate;
            this.Gender = gender;
            this.Class = priceClass;
        }

        public int PassengerId { get; set; }

        public string Passport { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Nationality { get; set; }

        public int? FlightNumber { get; set; }

        public DateTime BirthDate { get; set; }

        public Gender Gender { get; set; }

        public PriceClass? Class { get; set; }
    }
}
