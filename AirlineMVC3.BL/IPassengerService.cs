﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirlineMVC3.BL
{
    public interface IPassengerService : IService<PassengerBLModel>
    {
        IServiceResult<IEnumerable<PassengerBLModel>> GetPassengersByFlightNumber(string fltNum);
        IServiceResult<IEnumerable<PassengerBLModel>> GetPassengerByPassport(string passport);
        IServiceResult<IEnumerable<PassengerBLModel>> GetPassengerByFirstName(string fName);
        IServiceResult<IEnumerable<PassengerBLModel>> GetPassengerByLastName(string lName);
    }
}
