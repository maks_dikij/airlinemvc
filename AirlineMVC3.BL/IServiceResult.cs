﻿using System;

namespace AirlineMVC3.BL
{
    public interface IServiceResult<TData> : IServiceResult
    {
        TData Data { get; }
    }

    public interface IServiceResult
    {
        string Message { get; }
        bool IsSuccess { get; }
        Exception Exception { get; }
    }
}
