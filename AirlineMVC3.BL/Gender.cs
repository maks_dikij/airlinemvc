﻿namespace AirlineMVC3.BL
{
    public enum Gender
    {
        Male = 1,
        Female,
        NonBinary
    }
}
