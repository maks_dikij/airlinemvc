﻿using System.Collections.Generic;

namespace AirlineMVC3.BL
{
    public interface IService<TData>
    {
        IServiceResult<TData> Create(TData entity);
        IServiceResult<TData> Delete(int id);
        IServiceResult<TData> Get(int id);
        IServiceResult<IEnumerable<TData>> GetAll();
        IServiceResult<TData> Update(TData entity);
    }
}
