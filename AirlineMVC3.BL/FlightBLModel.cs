﻿using System;

namespace AirlineMVC3.BL
{
    public class FlightBLModel
    {
        public FlightBLModel()
        {

        }

        public FlightBLModel(int fltNum, FlightType fltType, DateTime dateTime, string port, int terminal, string company, decimal bisPrice, decimal ecPrice, FlightStatus fltStat)
        {
            this.FlightNumber = fltNum;
            this.FlightType = fltType;
            this.FLDate = dateTime;
            this.Port = port;
            this.Terminal = terminal;
            this.Company = company;
            this.BusinessPrice = bisPrice;
            this.EconomPrice = ecPrice;
            this.FlightStatus = fltStat;
        }
        public int FlightNumber { get; set; }

        public FlightType FlightType { get; set; }

        public DateTime FLDate { get; set; }

        public string Port { get; set; }

        public int Terminal { get; set; }

        public string Company { get; set; }

        public decimal BusinessPrice { get; set; }

        public decimal EconomPrice { get; set; }

        public FlightStatus FlightStatus { get; set; }

    }
}
