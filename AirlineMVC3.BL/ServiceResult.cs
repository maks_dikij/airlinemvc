﻿using System;

namespace AirlineMVC3.BL
{
    public class ServiceResult<TData> : IServiceResult<TData>
        where TData : class
    {
        private readonly TData data;
        private readonly string message;
        private readonly bool isSuccess;
        private readonly Exception exception;

        public ServiceResult(bool isSucc, TData data, Exception except = null, string mess = "")
        {
            this.data = data;
            this.isSuccess = isSucc;
            this.exception = except;
            
            if(mess == "")
            {
                this.message = isSucc ? "Result: Success" : except.Message;
            }
            else
            {
                this.message = mess;
            }

        }

        public TData Data
            => data;

        public string Message
            => message;

        public bool IsSuccess
            => isSuccess;

        public Exception Exception
            => exception;

        public static ServiceResult<TData> Success(TData data)
            => new ServiceResult<TData>(true, data);
        public static ServiceResult<TData> Failure(Exception ex)
            => new ServiceResult<TData>(false, null, ex);
    }
}
