﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirlineMVC3.BL
{
    public interface IFlightService : IService<FlightBLModel>
    {
        IServiceResult<IEnumerable<FlightBLModel>> GetFlightsByPort(string port);
        IServiceResult<IEnumerable<FlightBLModel>> GetFlightByPrice(string price);
        IServiceResult<IEnumerable<FlightBLModel>> GetFlightByNumber(string id);
    }
}
